(setq org-publish-project-alist
      '(("cdp-man"
         :base-directory "~/dev/cheredeprince.net/man/"
         :base-extension "org"
         :publishing-directory "/scp:cdp:/var/www/buron.coffee/book/man"
         :publishing-function org-html-publish-to-html
         :section-numbers nil
         :table-of-contents nil)))
